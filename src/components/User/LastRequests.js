import React, {Component} from 'react';
import {ActivityIndicator, View} from 'react-native'
import {Image, Text} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid'
import ProfilePic from "../Profile/ProfilePic";
import Icon from 'react-native-vector-icons/FontAwesome'

export default class LastRequests extends Component {

    constructor(props) {
        super(props);

        this.state = {
            services: [],
            loadingLastServices: true,
        }
    }

    componentWillMount() {

        this.getLastServices();
        this.loadLastServices();
    }

    getLastServices() {

        return fetch('http://127.0.0.1:8000/api/v1/services/last', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {

                console.warn(responseJson);

                this.setState({
                    services: responseJson,
                    loadingLastServices: false,
                })
            })
            .catch((error) =>    {
                console.warn('Deu erro no LS mano: ' + error);
            });
    }

    loadLastServices() {

        return this.state.services.map(service => (

            <Row style={{paddingBottom: 8, marginBottom: 5}}>
                <View style={{
                    flex: 1,
                    height: 80,
                    padding: 15,
                    paddingBottom: 5,
                    borderRadius: 10,
                    boxShadow: 2,
                    shadowOffset: 2,
                    backgroundColor: '#f7f7f7',
                }}>

                    <Grid style={{flex: 1, justifyContent: 'space-between',}}>
                        <Col size={20}>
                            <Row>
                                <ProfilePic/>
                            </Row>
                        </Col>

                        <Col size={80}>
                            <Row>
                                <Text style={{fontSize: 20, fontWeight: 'bold'}}>{service.nome_razao_social}</Text>
                            </Row>

                            <Row>
                                <Text style={{fontSize: 15,}}>Exemplo de Serviço</Text>
                            </Row>
                        </Col>

                    </Grid>


                </View>
            </Row>
        ));
    }

    loading() {

        if (this.state.loadingLastServices) {

            return (
                <Grid style={{flex: 1, marginTop: 36, marginBottom: 36}}>
                    <Row style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Col>
                            <ActivityIndicator size="large" color='#FF6600'/>
                        </Col>
                    </Row>
                </Grid>
            )
        }
    }

    render() {

        return (

            <Row size={40}>
                <Grid style={{ paddingTop: 8}}>
                    {this.loading()}

                    {this.state.loadingLastServices === false &&

                        this.loadLastServices()
                    }
                </Grid>
            </Row>
        )
    }
}