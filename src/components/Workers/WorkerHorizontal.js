import React, {Component} from 'react'
import {Image, ScrollView, View} from 'react-native';
import {Text} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid';
import {ActivityIndicator,} from "react-native";
import {withNavigation} from "react-navigation";


export class WorkerHorizontal extends Component {

    constructor(props) {
        super(props)

        const {navigation} = this.props;

        this.state = {
            categories: [],
            loadingCategories: true,
            workers: [],
        }
    }

    componentWillMount() {

        this.getCategories();
        this.loadCategories();
    }


    getCategories() {

        return fetch('http://127.0.0.1:8000/api/v1/categories/all', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {

                console.warn(responseJson);

                this.setState({
                    categories: responseJson,
                    loadingCategories: false,
                })
            })
            .catch((error) => {
                console.warn('Deu erro nas Cat mano: ' + error);
            });
    }

    loadCategories() {

        return this.state.categories.map(category => (


            <Grid style={{marginLeft: 15}} onPress={() => this.props.navigation.navigate('WorkerList', {
                categoria: category,
            })}>
                <Row size={90} style={{marginBottom: 10}}>
                    <Image source={{uri: 'http://127.0.0.1:8000' + category.url_icone}}
                           style={{
                               height: 70,
                               width: 70,
                           }}
                           PlaceholderContent={<ActivityIndicator/>}
                    />
                </Row>
                <Row size={10} style={{justifyContent: 'center'}}>
                    <Text style={{color: '#898989'}}>{category.titulo}</Text>
                </Row>
            </Grid>

        ));
    }

    loading() {

        if (this.state.loadingCategories) {

            return (
                <Grid style={{flex: 1, marginTop: 36, marginBottom: 36}}>
                    <Row style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Col>
                            <ActivityIndicator size="large" color='#FF6600'/>
                        </Col>
                    </Row>
                </Grid>
            )
        }
    }

    render() {

        return (
            <Row size={20} style={{marginTops: 10, justifyContent: 'space-between', alignItems: 'center'}}>
                <Col size={100}>
                    <View style={{flex: 1}}>
                        {this.loading()}

                        {this.state.loadingCategories === false &&

                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{flex: 1}}>
                            <Grid style={{padding: 5, paddingLeft: 0}}>
                                {this.loadCategories()}
                            </Grid>
                        </ScrollView>
                        }
                    </View>
                </Col>
            </Row>
        )
    }
}

export default withNavigation(WorkerHorizontal);
