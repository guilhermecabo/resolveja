import React, {Component} from 'react';
import {Card, CardItem, Container, Body, Text, Title, Left, Right, Button, Header} from "native-base";
import {Col, Row, Grid} from 'react-native-easy-grid'
import ProfilePic from "../Profile/ProfilePic";
import Icon from 'react-native-vector-icons/FontAwesome';
import {withNavigation} from "react-navigation";
import {StatusBar, ScrollView, ActivityIndicator} from 'react-native';

export class WorkerList extends Component {

    constructor(props) {

        super(props);

        this.state = {
            categoria: this.props.navigation.getParam('categoria'),
            workers: [],
            loadingWorkers: true
        }
    }


    componentWillMount() {

        this.getWorkers(this.state.categoria.id);
        this.loadWorkers();
    }

    getWorkers(category_id) {

        return fetch('http://127.0.0.1:8000/api/v1/categories/workers/' + category_id, {
            method: 'GET',
            headers: {
                Accept: 'application/json'
            }
        })
            .then(response => response.json())
            .then(responseJson => {

                this.setState({
                    workers: responseJson,
                    loadingWorkers: false
                });

                console.warn(this.state.workers);
            })
            .catch((error) => {
                console.warn('Erro no request')
            })
    }

    loadWorkers() {

        return this.state.workers.map(worker => (
                <Row size={30} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Card style={{margin: 5, flex: 1, height: 90}}>
                        <CardItem>
                            <Col size={20}>
                                <ProfilePic/>
                            </Col>
                            <Col size={60}>
                                <Text style={{
                                    fontSize: 18,
                                    fontWeight: 'bold',
                                    color: '#565656'
                                }}>{worker.nome_razao_social}</Text>
                            </Col>
                            <Col size={10}>
                                <Icon name='star' style={{color: '#e8ea1e', fontSize: 21}}/>
                            </Col>
                            <Col size={10}>
                                <Text style={{fontSize: 21}}>{worker.pontuacao.toFixed(1)}</Text>
                            </Col>
                        </CardItem>
                    </Card>
                </Row>
            )
        );
    }

    loadingWorkers() {
        if (this.state.loadingWorkers) {

            return (
                <Grid style={{flex: 1, marginTop: 180, marginBottom: 40,}}>
                    <Row style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Col>
                            <ActivityIndicator size="large" color='#FF6600'/>
                        </Col>
                    </Row>
                </Grid>
            )
        }
    }

    render() {

        return (
            <Container style={{backgroundColor: '#ff6700'}}>
                <Header style={{
                    backgroundColor: '#ff6700',
                    alignItems: 'center',
                    justifyContent: 'center',
                }} transparent>
                    <StatusBar barStyle='light-content' backgroundColor='#de5600'/>
                    <Left>
                        <Button transparent onPress={() => {
                            this.props.navigation.goBack()
                        }} style={{width: 80}}>
                            <Icon name='arrow-left' style={{fontSize: 22, color: '#fff'}}/>
                        </Button>
                    </Left>
                    <Body>
                    <Title>{this.state.categoria.titulo}</Title>
                    </Body>
                </Header>

                <Grid style={{marginTop: 5, backgroundColor: '#ebe9e4'}}>
                    <ScrollView showVerticalScrollIndicator={false} style={{flex: 1}}>
                        <Row>


                            {this.loadingWorkers()}

                            {this.state.loadingWorkers === false &&

                            <Grid style={{padding: 10,}}>
                                {this.loadWorkers()}
                            </Grid>
                            }

                        </Row>
                    </ScrollView>
                </Grid>
            </Container>

        )
    }
}

export default withNavigation(WorkerList);