import React, {Component} from 'react';
import { ActivityIndicator , AppRegistry, Image, View, Text} from 'react-native';
import {Col} from "react-native-easy-grid";

export default class ProfilePic extends Component {

    render() {

        return (
                <Image source={require('../../images/user-icon.png')}
                       style={{
                           borderWidth: 0.1,
                           borderRadius: 50,
                           height: 50,
                           width: 50,
                       }}

                       PlaceholderContent={<ActivityIndicator />}
                />

        )
    }
}