import React, {Component} from 'react';
import {StatusBar, View} from 'react-native';
import {
    Container,
    Content,
    List
} from "native-base";
import {Col, Row, Grid} from "react-native-easy-grid";
import ProfilePic from "./ProfileBar";
import {ListItem, Text} from "react-native-elements";

export default class SideNav extends Component {


    render() {

        return (
            <Container>
                <Content>
                    <Grid>
                        <Row style={{alignContent: 'center', justifyContent: 'center'}}>
                            <Col>
                                <ProfilePic style={{flex: 1}}/>
                            </Col>
                            <Col>
                                <Text>Usuário</Text>
                            </Col>
                        </Row>
                        <Row>
                            <List>
                                <ListItem>
                                    <Text>Perfil</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>Pedidos Feitos</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>Cupons</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>Formas de Pagamento</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>Configurações</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>Ajuda</Text>
                                </ListItem>
                                <ListItem>
                                    <Text>Seja um Prestador</Text>
                                </ListItem>
                            </List>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}
