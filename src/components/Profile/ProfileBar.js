import React, {Component} from 'react';
import {AppRegistry, View, TextInput, StatusBar} from 'react-native';
import ProfilePic from './ProfilePic';
import {Container, Header, Item, Input, Icon} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {withNavigation} from "react-navigation";
import {WorkerList} from "../Workers/WorkerList";


export class ProfileBar extends Component {

    state = {
        search: '',
    };

    updateSearch = search => {
        this.setState({search});
    };

    render() {
        const {search} = this.state;

        return (

                <Header searchBar rounded style={{backgroundColor: '#ff6700',}} transparent>
                    <StatusBar barStyle='light-content' backgroundColor='#de5600'/>
                    <Row style={{borderBottom: 1, borderColor: 'grey', backgroundColor: '#ff6700',}}>
                        <Col size={20} style={{alignItems: 'flex-start', backgroundColor: '#ff6700',  paddingTop: 5}}>
                            <Row>
                                <ProfilePic style={{flex: 1}}/>
                            </Row>
                        </Col>

                        <Col size={80} style={{paddingTop: 5}}>
                            <Item>
                                <Icon name='ios-search'/>
                                <Input placeholder='Pesquisar...'/>
                            </Item>
                        </Col>
                    </Row>
                </Header>
        );
    }
}

export default withNavigation(ProfileBar);