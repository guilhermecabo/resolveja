import React, {Component} from 'react';
import {ScrollView, View, StatusBar} from "react-native";
import ProfileBar from "../components/Profile/ProfileBar";
import {Text} from 'react-native-elements'
import {Col, Row, Grid} from "react-native-easy-grid";
import WorkerHorizontal from "../components/Workers/WorkerHorizontal";
import LastRequests from "../components/User/LastRequests";
import {Container, Drawer} from "native-base";
import SideNav from "../components/Profile/SideNav";
import {DrawerNavigator} from "react-navigation";

// const HomeScreenRouter = DrawerNavigator(
//     {
//         Home: {Home}
//     },
//     {
//         contentComponent: props => <SideNav {...props} />
//     }
// );

export default class Home extends Component {

    render() {
        return (
            <Container>
                <ProfileBar/>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Grid style={{backgroundColor: '#ff6700', paddingTop: 5}}>
                        <Row style={{backgroundColor: '#ff6700'}}>
                            <Col>
                                {/*<View style={{backgroundColor: '#ff6700', marginBottom: 1, padding: 10, alignItems: 'center'}}>*/}
                                {/*<ProfileBar/>*/}
                                {/*</View>*/}

                                <View style={{backgroundColor: '#fdfdfd', marginBottom: 3, padding: 10, paddingTop: 0}}>
                                    <Text h4 style={{marginTop: 20, textAlign: 'left', flex: 1, marginBottom: 15}}>
                                        Categorias
                                    </Text>
                                    <WorkerHorizontal/>
                                </View>

                                <View style={{flex: 1, height: 5, backgroundColor: '#ff6700'}}/>

                                <View style={{backgroundColor: '#ffffff', padding: 10,}}>
                                    <Text style={{
                                        marginTop: 20,
                                        marginBottom: 20,
                                        fontSize: 20,
                                        fontWeight: 'bold',
                                        textAlign: 'left',
                                        flex: 1,
                                    }}>Últimos
                                        Contratados</Text>

                                    <View style={{flex: 1, backgroundColor: 'black', height: 0.5, marginBottom: 7}}/>

                                    <LastRequests/>
                                </View>
                            </Col>

                        </Row>
                    </Grid>
                </ScrollView>
            </Container>
        )
    }
}