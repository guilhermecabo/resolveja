import React, {Component} from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Home from "./src/screens/Home";
import {Animated, Easing, View, StatusBar} from 'react-native';
import ProfileBar from "./src/components/Profile/ProfileBar";
import WorkerList from "./src/components/Workers/WorkerList";

const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 750,
            easing: Easing.out(Easing.poly(4)),
            timing: Animated.timing,
            useNativeDriver: true,
        },

        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps

            const thisSceneIndex = scene.index
            const width = layout.initWidth

            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            })

            return { transform: [ { translateX } ] }
        },
    }
}

const AppNavigator = createStackNavigator(
    {
        Home: Home,
        WorkerList: WorkerList,
    },
    {
        initialRouteName: 'Home',
        defaultNavigationOptions: {
            header: null
        },
        transitionConfig,
    }
);

console.disableYellowBox = true;

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {

    render() {
        return (

                <View style={{flex: 1, justifyContent: 'center', backgroundColor: '#ff6700', paddingRight: 2, paddingLeft: 2}}>
                    <StatusBar barStyle={'dark-content'} backgroundColor="#ff6700"/>
                    <AppContainer/>
                </View>
        )
    }
}



